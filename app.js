const validator = require('validator');
const yargs = require('yargs');
yargs.version('1.1.0'); // talkes single quotesonly
console.log(validator.isEmail("aaa@sss.com"));
//so here the third arg will be always
//the param that we passed while running the file
//thru the node comand
// so based on that comand we can set what operations we need
// to take for certain things.
//console.log(process.argv);
// we allso can give more comands and then we can parse those
// using new library. yargs 
//console.log(yargs.argv);

// create notes comand
yargs.command({
    command:'add',
    describe:'add a new note',
    builder:{
        title:{
            describe:"note title",
            demandOption:true,
            type:"string"
        },
        body:{
            describe:"note body",
            demandOption:true,
            type:"string"
        }
    },
    handler:function(argv){
        console.log('Note Title :::  ',argv.title,"Note Body :::",argv.body)
    }
    
});

yargs.command({
    command:'remove',
    describe:'remove a new note',
    handler:function(){
        console.log('Notes Removed !!! ')
    }
    
});

yargs.command({
    command:'read',
    describe:'read a new note',
    handler:function(){
        console.log('Notes Read !!! ')
    }
    
});

yargs.command({
    command:'list',
    describe:'list a new note',
    handler:function(){
        console.log('Notes List !!! ')
    }
    
});


//the below line is needed to list the commands.
//yargs.argv;
yargs.parse();



